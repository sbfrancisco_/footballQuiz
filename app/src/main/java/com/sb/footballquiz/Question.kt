package com.sb.footballquiz

import androidx.annotation.StringRes

/**
 * Created by Sb on 17/06/2020
 * com.sb.footballquiz
 * FootballQuiz
 */
data class Question(@StringRes val textResId: Int, val answer: Boolean)